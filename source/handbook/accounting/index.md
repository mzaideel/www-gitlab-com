---
layout: markdown_page
title: "Accounting"
---

## Payroll

### Gitlab, Inc. Payroll Procedures
1. You will received a reminder from Trinet the week before payroll closes for each pay period. 

1. If you are handling payroll changes- you the email will let you know by what date you must submit those changes.

1. Enter the dates into your calendar with a reminder for the day before the deadline

1. Log into TriNet Passport

1. My Company=>My Workplace=>Payroll Entry & Admin

1. You are now on the payroll dashboard

1. Select the current pay period from the calendar screen in the middle of the page.

1. Each employee will have a row with the information necessary to document hours (if hourly) and other earnings (this can be commission, reimbursement, etc. There is a drop down menu of options and codes from which to choose).

1. After you have updated each employee, select the save button.

1. Do not use the Submit button. It will make adding last minute changes difficult and TriNet will take your last save when processing payroll.

1. Make sure that you calculate the hours based on the pay days (count them) for each pay period.

### GitLab, BV Pay Slip Distribution Process

1. Save the attachments that arrive from Loonbureau each month to a folder in DropBox that you label like this: Dropbox=> GitLab BV=>year=>month (the entire attachment goes into this folder).

1. On or before the 25th of each month, send each employee their salary slip (see steps below) and a note to the CEO to make sure that he processes the necessar paperwork.

1. Following the path above, use the salary document to generate each employees salary slip. The dutch file for salaries is called -Salarisstroken.pdf

1. Each page is a different employees salary slip all on one document. Do NOT send anyone a salary slip that is not their own! Follow the next steps carefully. 

1. Work on each page one at a time by

1. Select, highlight and right click to copy the name of the employee

1. Select file/print from the drop down menu

1. Select the pages you want to print (this is where it is important to do ea page one at a time - ex. 1-1,2-2. 

1. Select save as PDF from the bottom left hand side of the page (you must do this step after the previous step or it will SAVE all pages).

1. When the save as screen appears, paste the name from above and add month/year into the save as window.

1. Save the document to someplace convenient for sharing with them individually - ex. July BV payroll in a private folder (this is only for sharing with individuals, it is not the copy of record).

1.In the Title section: salary slip and past the name/month/year again

1. Save

ALWAYS confirm each saved page is only 1 employee - or you will send employees other people's salary slip.

Send each employee their salary slip via email with the subject line: Salary slip

### Additional Begin/End Payment (for adding payment to payroll)

1. In HR Passport click Find 

1. Select find by Name 

1. Click on Add’l Begin/End Payment 

1. Select Action Type 

1. Select Begin & End date (End date is optional.  Note: The Begin date needs to be the start of a pay period,  it cannot be in the middle or end of a pay period) 

1. Enter Goal Amount (if applicable) 

1. Enter the Earn Type 

1. Enter the amount per pay period

1. Enter pay frequency


## Accounts payable

NOTE: Vendor invoices are to be sent to accountspayable@gitlab.com

Steps to process an invoice to pay a consultant/vendor:

1. Upon receipt of vendor invoices:
    * File a .pdf copy of the invoice to dropbox\For Approval.
    * Notify manager of new invoices to be approved by forwarding the email from the vendor.
    * Invoices are to be approved based on signed agreements, contract terms, and or purchase orders.
    * After review, manager to reply to email with “Approved”. An audit trail is required and this email will serve this purpose.

1. On approval, move the invoice from dropbox\For Approval to dropbox\Inbox

1. Post the invoice through accounting system.  Before paying any vendor, be sure there is a W-9 on file for them (Inc.vendors only).

1. On a daily basis, generate an AP aging summary from the accounting system and identify invoices to be paid.

1. Initiate payment(s) through the bank (Comerica/Rabobank) and notify management that there is a pending payment.  Include a summary of invoices being paid.

1. Verify the payment has cleared the bank.

1. Upon verified payment of the invoice move the .pdf copy of the invoice from dropbox\Inbox to folder inbox\”vendor name”.

1. Post the payment through the accounting system.

## Commission Payment Process

1. Each sales person will receive their own calculation template.

1. Salesperson is to complete their monthly template four days (payroll will send reminder) prior to first payroll of the month. Upon completion, salesperson will ping a manager for review and approval.

1. Approving manager will ping accounting upon approval.

1. Accounting will review and reconcile paid vs unpaid invoices.

1. Accounting will note in calculation template the amounts to be paid in commision.

1. Accounting will ping payroll that commission calculation is complete.

1. Payroll will enter commission into TriNet.

## Company Credit Cards

1. All team members that regularly make company purchases may request a company credit card. 

1. You are personally responsible to make sure that the charges on the card are for GitLab business only.

1. Please file your expense report on time. 

1. Those who need a company credit card will be provided with additional directions for card management and reporting.

1. If you request a company credit card for yourself or a direct report, please make sure that you are added to Expensify for monthly reporting by sending an email to finance@gitlab.com